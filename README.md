# probacseq-toolkit

## Installation
Copy the file to the working directory with:
`git clone https://gitlab.com/hormozlab/probacseq-toolkit.git`

## example
An example of single-cell multiplexing data processing is available at [here](https://gitlab.com/hormozlab/probacseq-toolkit/-/blob/main/example.ipynb)
